package main

import (
	"GRPC-GOkos/room/client/building_client"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main()  {
	fmt.Println("Server is Running")
	lis,err:=net.Listen("tcp","0.0.0.0:5002")
	if err != nil {
		log.Fatalf("Error Listener: %v",err)
	}

	building_client.Dial()

	runServer(lis)
}

func runServer(listener net.Listener)  {
	gs:=grpc.NewServer()
	gs.Serve(listener)
}