package building_client

import (
	"GRPC-GOkos/room/proto/buildingpb"
	"context"
	"fmt"
	"google.golang.org/grpc"
	"io"
	"log"
)

func Dial()  {
	conn,err:=grpc.Dial("0.0.0.0:5001",grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v",err)
	}
	fetch(conn)
}

func fetch(conn *grpc.ClientConn)  {
	sc:=buildingpb.NewBuildingServiceClient(conn)
	s,err:=sc.Fetch(context.Background(),&buildingpb.Empty{})
	if err != nil {
		log.Fatalf("error req: %v",err)
	}

	for  {
		res,err:=s.Recv()
		if err==io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("error res: %v",err)
		}

		fmt.Println(res.Buildings.ListTags.ListTag[0])
	}
}